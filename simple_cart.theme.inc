<?php
/*
 * @file
 * simple_cart.theme.inc Contains theme-functions
 */

/**
 * Theme the list of nodes in cart.
 * Will be used for block which shows cart content.
 *
 * @param array $list Associated array when key is nid and value - some node's information.
 * @param boolean $show_remove_link TRUE if remove link should be shown.
 * @param boolean $show_add_link TRUE if add link should be shown.
 * @param boolean $show_item_quantity TRUE if item's quantity should be shown.
 * @return string Rendered list of nodes in cart.
 */
function theme_cart_items_list($list=array()) {
  $formatted = array();
  $total_price = 0;
  $attributes = array('id' => 'cart-list-block');
  $cart_is_empty = TRUE;
  $hide = 'style="display:none"';
  $rendered_items_list = '<div class="item-list"><ul id="cart-list-block"></ul></div>';
  if (is_array($list['items']) && count($list['items']) > 0) {
    $cart_is_empty = FALSE;
    foreach ($list['items'] as $nid => $node_data) {
      $formatted[] = theme('cart_block_item', $node_data, $use_destination);
    }
    $total_price = check_plain($list['total_price']);
    $rendered_items_list = theme('item_list', $formatted, NULL, 'ul', $attributes);
  }
  return '<span ' . ($cart_is_empty ? '' : $hide) . ' id="cart-block-empty-message">' 
    . t('Your cart is empty.') . '</span>' . $rendered_items_list
    . '<div ' . ($cart_is_empty ? $hide : '') . ' id="cart-block-total"><strong>'
    . t('Total:') . ' <span id="cart-block-total-price">'
    . format_number($total_price, 2) . '</span> $</strong></div>';
}

/**
 * Theme cart's item for block with items list.
 */
function theme_cart_block_item($node_data=array()) {
  $output = '';
  if (count($node_data) > 0) {
    $output .= l($node_data['title'], 'node/' . $node_data['nid'],
      array(
        'attributes' => array(
          'class' => 'cart-list_block-item',
          'id'    => 'cart-block-item-' . check_plain($node_data['nid'])
        )
      )
    );
    if (variable_get('simple_cart_block_show_item_quantity', TRUE)) {
      $output .= ' <strong>x <span class="cart-item-quantity">'
                 . check_plain($node_data['quantity']) . '</span></strong>';
    }
    $output .= theme('cart_links', $node_data['nid'], $use_destination);
  }
  return $output;
}

/**
 * Theme 'Add to Cart' and 'Remove From Cart' links.
 *
 * @param integer $nid Nodes ID.
 * @param boolean $show_remove_link TRUE if 'Remove From Cart' link should be added.
 * @param boolean $show_add_link TRUE if 'Add To Cart' link should be added.
 * @return string Returns rendered HTML with requested buttons code.
 */
function theme_cart_links($nid='') {
  if ($nid) {
    $destination = drupal_get_destination();
    $links = array();

    if (variable_get('simple_cart_block_show_quantity_update_buttons', TRUE)) {
      $links[] = l(t('+1'), 'cart/add/' . $nid . '/1',
        array(
          'query' => $destination,
          'attributes' => array('class' => 'simple-cart-button', 'title' => "Change item's quantity")))
        . '<span style="display:none" class="simple-cart-throbber">'. t('Working...') . '</span>';
      $links[] = l(t('-1'), 'cart/add/' . $nid . '/-1',
        array(
          'query' => $destination,
          'attributes' => array('class' => 'simple-cart-button', 'title' => "Change item's quantity")))
        . '<span style="display:none" class="simple-cart-throbber">'. t('Working...') . '</span>';
    }
    // add a remove link
    if (variable_get('simple_cart_block_show_remove_button', TRUE)) {
      $links[] = l(t('Remove'), 'cart/remove/' . $nid,
        array(
          'query' => $destination,
          'attributes' => array('class' => 'simple-cart-button')))
        . '<span style="display:none" class="simple-cart-throbber">'. t('Working...') . '</span>';
    }
    if (count($links)) {
      return ' ' . implode(' | ', $links);
    }
  }
}

/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_cart_items_form($form) {
  $path = drupal_get_path('module', 'simple_cart');
  drupal_add_css($path . '/simple_cart.css');
  $rows = array();
  $type_names = node_get_types('names');
  $type = '';
  foreach (element_children($form['checkboxes']) as $nid) {
    $row = array();
    if ($type != $form[$nid]['node_type']['#value']) {
      $type = $form[$nid]['node_type']['#value'];
      // Insert subheader:
      $rows[] = array(
        'data' => array(
          array('data' => $type_names[$type], 'colspan' => 5, 'class' => 'node-type-subheader'),
        ),
      );
    }
    $row[] = drupal_render($form['checkboxes'][$nid]);
    $row[] = drupal_render($form[$nid]['teaser']);
    $row[] = array(
      'data' => drupal_render($form[$nid]['price']) . ' $',
      'class' => 'price',
    );
    $row[] = drupal_render($form['quantity']['quantity_' . $nid]);

    $row[] = array(
      'data' => drupal_render($form[$nid]['item_amount']) . ' $',
      'class' => 'item-amount',
    );
    $rows[] = $row;
  }
  if (count($rows)) {
    $remove_all_header = theme('table_select_header_cell');
    $header = array($remove_all_header, t('Items'), t('Item Price'), t('Quantity'), t('Amount'));
    $rows[] = array(
      'data' => array(
        array('data' => t('Total:'), 'colspan' => 4, 'class' => 'total_price_label'),
        // @TODO: replace with real price:
        array('data' => drupal_render($form['total_price']) . ' $', 'class' => 'total_price_amount'),
      ),
    );
    $output = theme('table', $header, $rows);
    return $output . drupal_render($form);
  }
  else {
    return '<p class="empty-cart">' . t('You have empty cart.') . '</p>';
  }
}



/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_checkout_cart_items($list) {
  if (count($list) > 0) {
    $path = drupal_get_path('module', 'simple_cart');
    drupal_add_css($path . '/simple_cart.css');

    $rows = array();
    foreach ($list as $nid => $node_data) {
      $node = node_load($nid);
      $node->build_mode = 'checkout';
      $row = array();
      $row[] = node_view($node, TRUE, $page, FALSE);
      $row[] = check_plain($node_data['quantity']);
      $row[] = check_plain($node_data['price']) . ' ' . t('$');
      $total_price += $node_data['price'] * $node_data['quantity'];
      $rows[] = $row;
    }
    if (count($rows)) {
      $header = array(t('Items'), t('Quantity'), t('Price'));
      $rows[] = array(
        'data' => array(
          array('data' => t('Total:'), 'colspan' => 2, 'class' => 'total_price_label'),
          array('data' => $total_price . ' $', 'class' => 'total_price_amount'),
        ),
      );
      return theme('table', $header, $rows);
    }
    else {
      return '<p class="empty-cart">' . t('You have empty cart.') . '</p>';
    }
  }
}


/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $list Array of nodes in cart. Key is a node's ID and value contain some node's data.
 * @param string $build_mode Node build mode. Can be 'customer_email' or 'manager_email'. But actualy can have other values 'teaser', 'full', 'rss' and etc.
 * @return string Rendered list of nodes in cart.
 */
function theme_email_cart_items($build_mode='customer_email') {
  $list = simple_cart_get_content_list();
  if (count($list['items']) > 0) {
    $output = array();
    $output[] = '===================================================';
    $output[] = t('Cart items');
    $output[] = '===================================================';
    foreach ($list['items'] as $nid => $node_data) {
      $node = node_load($nid);
      $node->build_mode = $build_mode;
      $output[] = t('Item:');
      $output[] = check_plain($node->title);
      $output[] = t('URL:') . ' '. url('node/' . $node->nid, array('absolute' => TRUE));
      //$output[] = node_view($node, TRUE, $page, FALSE);
      //$output[] = $node->teaser; // Gives a lot of tags so can't be used
      $output[] = t('Quantity:') . ' ' . check_plain($node_data['quantity']);
      $output[] = t('Price:') . ' ' . check_plain($node_data['price']) . ' $';
      $output[] = '===================================================';
      $total_price += $node_data['price'] * $node_data['quantity'];
    }
    if (function_exists('format_number')) {
      $total_price = format_number(check_plain($total_price), 2);
    }
    if (count($output)) {
      $output[] = t('Total:') . ' ' . $total_price . ' $';
      $output[] = '===================================================';
      return implode("\n", $output);
    }
    else {
      return t('You have empty cart.');
    }
  }
}

/**
 * Render Add to cart|Remove from cart buttons for node's links.
 *
 * @param object $node
 * @return string Returns rendered HTML code.
 */
function theme_add_to_cart_link($node) {
  $output = '';
  $buttons = array (
    'remove_link' => array(
      'anchor'      => variable_get('simple_cart_remove_from_cart_button_text' . $node->type, t('Remove from Cart')),
      'url'         => 'cart/remove/' . $node->nid,
      'attributes'  => array('class' => 'simple-cart-button remove-link'),
    ),
    'add_link' => array(
      'anchor'      => variable_get('simple_cart_add_to_cart_button_text' . $node->type, t('Add to Cart')),
      'url'         => 'cart/add/' . $node->nid . '/1',
      'attributes'  => array('class' => 'simple-cart-button add-link'),
    ),
  );
  $node_in_cart = simple_cart_get_node_quantity($node->nid);
  if ($node_in_cart) {
    $buttons['add_link']['attributes']['style'] = 'display: none;';
  }
  else {
    $buttons['remove_link']['attributes']['style'] = 'display: none;';
  }
  foreach ($buttons as $button_link) {
    $output .= l($button_link['anchor'], $button_link['url'],
      array('query' => drupal_get_destination(), 'attributes'  => $button_link['attributes'])
    );
    /* Throbber can be added using JS but this solution doesn't require
       JS to find, append and mark as proccessed posibly one element at page.
       So this is faster to add it here:
    */
    $output .= '<span style="display:none" class="simple-cart-throbber">'. t('Working...') . '</span>';
  }
  return $output;
}
