<?php
/*
 * @file
 * File contains fucntions for cart form which is shown at cart page.
 */

/**
 * Callback for menu item /cart.
 * Show cart content for currect user.
 *
 * @return string
 */
function simple_cart_cart_form() {
  $path = drupal_get_path('module', 'simple_cart');
  format_number_add_js();
  drupal_add_js($path . '/simple_cart.js');
  $total_price = 0;
  if ($selected_types = simple_cart_get_cartable_types()) {
    foreach ($selected_types as $type) {
      $items = simple_cart_get_content_by_type($type);
      if ($items) {
        foreach ($items as $nid => $data) {
          $node = node_load($nid);
          $node->build_mode = 'cart';
          $checkboxes[$nid] = '';
          $form[$nid]['teaser'] = array('#value' => node_view($node, TRUE, $page, FALSE)); // Show node teaser
          $form['quantity']['quantity_' . $nid] = array(
            '#default_value'  => check_plain($data['quantity']),
            '#type'           => 'textfield',
            '#size'           => 4,
            '#maxlength'      => 4,
            '#title'          => '',
            '#description'    => '',
          );
          $form[$nid]['price'] = array('#value' => format_number(check_plain($data['price']), 2));
          $form[$nid]['item_amount'] = array('#value' => format_number(check_plain($data['price'] * $data['quantity']), 2));
          $form[$nid]['node_type'] = array('#type' => 'hidden', '#value' => $type);

          $total_price += $data['price']*$data['quantity'];
        }
        $form['checkboxes'] = array(
          '#type'     => 'checkboxes',
          '#options'  => $checkboxes,
        );

        $form['remove'] = array(
          '#type'           => 'button',
          '#name'           => 'remove_selected',
          '#value'          => t('Remove selected'),
        );
        $form['submit'] = array(
          '#type'           => 'button',
          '#name'           => 'update_quantity',
          '#value'          => t('Update'),
        );
        $form['checkout'] = array(
          '#type'           => 'submit',
          '#name'           => 'checkout',
          '#value'          => t('Checkout'),
        );
      }
    }
  }
  if (function_exists('format_number')) {
    $total_price = format_number($total_price, 2);
  }
  $form['total_price'] = array('#value' => $total_price);

  // Placed out because this theme function also shows message for empty cart.
  $form['#theme'] = 'cart_items_form';
  return $form;
}

function simple_cart_cart_form_validate($form, $form_state) {
  $op = $form_state['clicked_button']['#name'];
  switch ($op) {
    case 'remove_selected':
      $items_was_removed = FALSE;
      foreach ($form_state['values']['checkboxes'] as $nid) {
        if ($nid) {
          simple_cart_remove_node($nid);
          $items_was_removed = TRUE;
        }
      }
      if ($items_was_removed) {
        drupal_set_message(t('Selected items has been removed from your cart.'));
        drupal_goto('cart');
      }
      break;
    case 'checkout':
      // Do update in cart. 'update_quantity' and 'checkout' uses the same code.
    case 'update_quantity':
      $cart_was_updated = FALSE;
      foreach ($form_state['values'] as $key => $quantity) {
        if (strpos($key, 'quantity_') !== FALSE) {
          $nid = drupal_substr($key, 9);
          // Check if recieved only numbers:
          if (!is_numeric($quantity)) {
            form_set_error('quantity_'. $nid , t('Quantity must be numeric.'));
          }
          else {
            simple_cart_update_node_quantity($nid, $quantity);
            $cart_was_updated = TRUE;
          }
        }
      }
      if ($cart_was_updated) {
        if ($op == 'update_quantity') {
          drupal_set_message(t('Quantity and total price has been updated.'));
          drupal_goto('cart');
        }
      }
      break;
  }
}

/**
 * Submit handler for form with ID = 'cart_items_form'.
 * Do the redirect to checkout page.
 *
 * @param array $form
 * @param array $form_state
 */
function simple_cart_cart_form_submit($form, $form_state) {
  if ($form_state['clicked_button']['#name'] == 'checkout') {
    drupal_goto('checkout');
  }
}
