<?php
/*
 * @file
 * simple_cart.rules.inc Integration with Rules module.
 *
 * Rules events ===============================================================
 */

/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function simple_cart_rules_event_info() {
  return array(
    // Adding to cart
    'simple_cart_before_add_to_cart' => array(
      'label' => t('Before node adding to cart'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
        'quantity' => array('type' => 'number', 'label' => t('Quantity.')),
        'quantity_in_cart' => array('type' => 'number', 'label' => t('Quantity in cart.')),
      ),
    ),
    'simple_cart_after_add_to_cart' => array(
      'label' => t('After node adding to cart'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
        'quantity' => array('type' => 'number', 'label' => t('Quantity.')),
        'quantity_in_cart' => array('type' => 'number', 'label' => t('Quantity in cart.')),
      ),
    ),
    // Node removing from cart
    'simple_cart_before_node_removing_from_cart' => array(
      'label' => t('Before node removing from cart'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
      ),
    ),
    'simple_cart_after_node_removing_from_cart' => array(
      'label' => t('After node removing from cart'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
      ),
    ),
    // Cart update (add, remove and quantity changes):
    'simple_cart_cart_update' => array(
      'label' => t('Cart updates'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
      ),
    ),
    // Removing all cart items
    'simple_cart_remove_all_items' => array(
      'label' => t('Removing all items from cart'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
      ),
    ),
    // Node quantity updates
    'simple_cart_before_node_quantity_updates' => array(
      'label' => t('Before updating node quantity'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
        'quantity' => array('type' => 'number', 'label' => t('Quantity.')),
        'quantity_in_cart' => array('type' => 'number', 'label' => t('Quantity in cart.')),
      ),
    ),
    'simple_cart_after_node_quantity_updates' => array(
      'label' => t('After updating node quantity'),
      'module' => 'Simple Cart',
      'arguments' => array(
        'customer' => array('type' => 'user', 'label' => t('Customer.')),
        'node_owner' => array('type' => 'user', 'label' => t("Node's author.")),
        'node' => array('type' => 'node', 'label' => t('Node.')),
        'quantity' => array('type' => 'number', 'label' => t('Quantity.')),
        'quantity_in_cart' => array('type' => 'number', 'label' => t('Quantity in cart.')),
      ),
    ),
    //
    //
    // TODO:
    // Checkout form submitted

    
    // Sending emails to customer
    // Sending emails to managers
  );
}

/*
 * Rules conditions ===========================================================
 */

/**
 * Implementation of hook_rules_condition_info().
 */
function simple_cart_rules_condition_info() {
  return array(
    'simple_cart_condition_node_in_cart' => array(
      'label' => t('Node is in cart'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Node')),
      ),
      'help' => t('Evaluates to TRUE, if node was already in cart.'),
      'module' => 'Simple Cart',
    ),
    'simple_cart_condition_node_is_cartable' => array(
      'label' => t('Node is cartable'),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Node')),
      ),
      'help' => t('Evaluates to TRUE, if node can be added to cart.'),
      'module' => 'Simple Cart',
    ),
    'simple_cart_condition_cart_is_empty' => array(
      'label' => t('Cart is empty'),
      'help' => t('Evaluates to TRUE, if current user cart is empty.'),
      'module' => 'Simple Cart',
    ),
    // Price ranges
    /*
     * Note: price field uses currency and can have different amount.
     * So currency support should be added first.
    'simple_cart_condition_price_less_than_or_equal ' => array(
      'label' => t("Node's price less-than or equal"),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Node')),
      ),
      'help' => t("Evaluates to TRUE, if node's price less-than or equal to given value."),
      'module' => 'Simple Cart',
    ),
     */
  );
}

/**
 * Returns TRUE if cart is empty and FALSE - if not.
 */
function simple_cart_condition_cart_is_empty() {
  return simple_cart_is_empty();
}

/**
 * Returns TRUE if node is cartable.
 */
function simple_cart_condition_node_is_cartable($node) {
  return simple_cart_node_is_cartable($node->type);
}

/**
 * Check if node is already in cart (returns TRUE).
 */
function simple_cart_condition_node_in_cart($node) {
  return simple_cart_get_node_quantity($node->nid);
}

/*
 * Rules actions ==============================================================
 */

/**
 * Implementation of hook_rules_action_info().
 * 
 * Note: all actions operates with cart of current user.
 *
 * @ingroup rules
 */
function simple_cart_rules_action_info() {
  // Add node to cart
  return array(
    'simple_cart_action_add_current_node_to_cart' => array(
      'label'       => t('Add current node to cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'node' => array(
          'type'          => 'node',
          'label'         => t('Content'),
          'description'   => t("Node, which will be added to cart."),
        ),
        'quantity' => array(
          'type'          => 'number',
          'label'         => t('Quantity'),
          'required'      => FALSE,
          'description'   => t("Quantity of node, which will be added to cart."),
          'default value' => '1',
        ),
      ),
    ),
    'simple_cart_action_add_any_node_to_cart' => array(
      'label'       => t('Add any node to cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'nid' => array(
          'type'          => 'number',
          'label'         => t('Content ID'),
          'required'      => TRUE,
          'description'   => t("If you want to add any node to cart, specify node's id."),
        ),
        'quantity' => array(
          'type'          => 'number',
          'label'         => t('Quantity'),
          'required'      => FALSE,
          'description'   => t("Quantity of node, which will be added to cart."),
          'default value' => '1',
        ),
      ),
    ),
    // Remove node from cart
    'simple_cart_action_remove_current_node_to_cart' => array(
      'label'       => t('Remove current node from cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'node' => array(
          'type'          => 'node',
          'label'         => t('Content'),
          'description'   => t("Node, which will be removed from cart."),
        ),
      ),
    ),
    'simple_cart_action_remove_any_node_to_cart' => array(
      'label'       => t('Remove any node from cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'nid' => array(
          'type'          => 'number',
          'label'         => t('Content ID'),
          'required'      => TRUE,
          'description'   => t("If you want to remove any node from cart, specify node's id."),
        ),
      ),
    ),
    // Empty cart
    'simple_cart_action_remove_all_cart_items' => array(
      'label'       => t('Empty cart'),
      'module'      => 'Simple Cart',
    ),
    // Update node's quantity
    'simple_cart_action_update_current_node_quantity' => array(
      'label'       => t('Update quantity of current node in cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'node' => array(
          'type'          => 'node',
          'label'         => t('Content'),
          'description'   => t("Node, which quantity will be updated."),
        ),
        'quantity_delta' => array(
          'type'          => 'string',
          'label'         => t('Quantity'),
          'description'   => t("Specify how should be changed node's quantity. For example, '+1' or '-5'."),
        ),
      ),
    ),
    'simple_cart_action_update_any_node_quantity' => array(
      'label'       => t('Update quantity of any node in cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'nid' => array(
          'type'          => 'number',
          'label'         => t('Content ID'),
          'required'      => TRUE,
          'description'   => t("If you want to add any node to cart, specify node's id."),
        ),
        'quantity_delta' => array(
          'type'          => 'string',
          'label'         => t('Quantity'),
          'description'   => t("Specify how should be changed node's quantity. For example, '+1' or '-5'."),
        ),
      ),
    ),
    // Set node's quantity in cart
    'simple_cart_action_set_current_node_quantity' => array(
      'label'       => t('Set quantity of current node in cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'node' => array(
          'type'          => 'node',
          'label'         => t('Content'),
          'description'   => t("Node, which quantity will be set."),
        ),
        'quantity' => array(
          'type'          => 'number',
          'label'         => t('Fixed quantity'),
          'description'   => t("quantity of node in cart will be changed to given value."),
        ),
      ),
    ),
    'simple_cart_action_set_any_node_quantity' => array(
      'label'       => t('Set quantity of any node in cart'),
      'module'      => 'Simple Cart',
      'arguments' => array(
        'nid' => array(
          'type'          => 'number',
          'label'         => t('Content ID'),
          'required'      => TRUE,
          'description'   => t("If you want to add any node to cart, specify node's id."),
        ),
        'quantity' => array(
          'type'          => 'number',
          'label'         => t('Fixed quantity'),
          'description'   => t("Quantity of node in cart will be changed to given value."),
        ),
      ),
    ),

  // TODO:
  // Email to managers
  // Email to customer
  );
}

/**
 * Action "Add current node to cart".
 */
function simple_cart_action_add_current_node_to_cart($node, $quantity) {
  if (is_object($node)) {
    simple_cart_action_add_any_node_to_cart($node->nid, $quantity);
  }
}

/**
 * Action "Add any node to cart".
 */
function simple_cart_action_add_any_node_to_cart($nid, $quantity) {
  if (is_numeric($nid)) {
    simple_cart_add_node($nid, $quantity);
  }
}

/**
 * Action "Remove current node from cart".
 */
function simple_cart_action_remove_current_node_to_cart($node) {
  if (is_object($node)) {
    simple_cart_action_remove_any_node_to_cart($node->nid);
  }
}

/**
 * Action "Remove any node from cart".
 */
function simple_cart_action_remove_any_node_to_cart($nid) {
  if (is_numeric($nid)) {
    simple_cart_remove_node($nid);
  }
}

/**
 * Action "Empty cart".
 */
function simple_cart_action_remove_all_cart_items() {
  simple_cart_remove_all_items();
}

/**
 * Action "Update quantity of current node in cart".
 */
function simple_cart_action_update_current_node_quantity($node, $quantity_delta) {
  if (is_object($node)) {
    simple_cart_action_update_any_node_quantity($node->nid, $quantity_delta);
  }
}

/**
 * Action "Update quantity of any node in cart".
 */
function simple_cart_action_update_any_node_quantity($nid, $quantity_delta) {
  if (is_numeric($nid)) {
    $quantity = simple_cart_get_node_quantity($nid);
    // Regexp used to have validation and some protection of values recieved from user.
    preg_match('/^([\+-])(\d+)/', check_plain($quantity_delta), $matches);
    $sign = $matches[1];
    $delta = $matches[2];
    if ($sign == '+') {
      $quantity += $delta;
    }
    elseif ($sign == '-') {
      $quantity -= $delta;
    }
    simple_cart_update_node_quantity($nid, $quantity);
  }
}

/**
 * Action "Set quantity of current node in cart".
 */
function simple_cart_action_set_current_node_quantity($node, $quantity) {
  if (is_object($node)) {
    simple_cart_action_set_any_node_quantity($node->nid, $quantity);
  }
}

/**
 * Action "Set quantity of any node in cart".
 */
function simple_cart_action_set_any_node_quantity($nid, $quantity) {
  if (is_numeric($nid)) {
    simple_cart_update_node_quantity($nid, $quantity);
  }
}
