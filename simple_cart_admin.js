
/*
 * @file
 * Contains JS code to handle 'Add to Cart' links using AJAX.
 */
Drupal.behaviors.SimpleCartAdmin = function (context) {
  $('.simple_cart_content_type_selector', context).change(function () {
    if ($(this).is(':checked')) {



      $('#' + $(this).attr('id') + '-fieldset', context)
        .show()
        .removeClass('collapsed');
    }
    else {
      $('#' + $(this).attr('id') + '-fieldset', context)
        .hide()
        .addClass('collapsed')
        .find('.simple_cart_price_field')
          .find('option:eq(0)').attr('selected', 'selected')
        .end().change();
    }
  });
  $('.simple_cart_price_field', context).change(function () {
    var $addToCartFieldset = $(this).parent().parent().find('.simple_cart_add_to_cart_fieldset');
    if ($(this).val() == '0') {
      $addToCartFieldset
        .hide()
        .find('.form-checkbox').removeAttr('checked');
    }
    else {
      $addToCartFieldset
        .show()
        .find('.form-checkbox').attr('checked', 'checked');
    }
  });
  // Fire up change event for each price selectbox:
  $('.simple_cart_content_type_selector', context).change();
  $('.simple_cart_price_field', context).change();
}
